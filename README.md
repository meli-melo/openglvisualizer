# Open GL Visualizer

Beginner OpenGL project to display models.

## Getting Started

Download this repository and open it in Visual Studio 2019. From there you can just start the project with "Local Windows Debugger".

### Prerequisites

To run this project you need OpenGL dlls, GLEW, GLFW, GLM and SOIL2. They should be included in the Linking folder except for OpenGL.


## Authors

* **Jean Bourquard** - *Whole project* - [meli-melo](https://gitlab.com/meli-melo)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
